<!--У двох рядках містяться дати виду День-Місяць-Рік (наприклад, 10-02-2015). Визначте кількість днів між датами.-->
<?php
$date1 = "10-02-2015";
$date2 = "20-02-2015";
$strtime1 = strtotime($date1);
$strtime2 = strtotime($date2);
$diff = $strtime2 - $strtime1;
$days_diff = floor($diff / (3600 * 24));
echo "Кількість днів між датами: " . $days_diff;
