<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
<?=
generateColoredTable(4, 4);?>
<span>Квадрати</span>
<div id="black_div" style = "position:relative">
<?=randomsquares(10)?>
</div>
</body>
</html>
<?php
function randomsquares($squares){
    for ($i=0; $i<$squares; $i++) {
        $randomsize = rand(5,100);
        echo"<div style='width: ".$randomsize."px; height: ". $randomsize."px; position: absolute; top: ".rand(0,500-$randomsize)."px; left: ".rand(0,500-$randomsize)."px; background-color: red; '>"; echo"</div>";
    }
}
function generateColoredTable($rows, $columns) {
    echo "<table>";
        for ($i = 1; $i <= $rows; $i++) {
        echo "<tr>";
            for ($j = 1; $j <= $columns; $j++) {
                $randomColor = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
                echo "<td style='background-color: $randomColor; color: black;'>Cell $i,$j</td>";
            }
            echo "</tr>";
        }
    echo "</table>";
    }
