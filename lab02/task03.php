<?php
$animalComponents = ['Лол', 'Ага', 'Пер', 'Дюк', 'Стар', 'Чаппі'];
function findDuplicates($arr) {
    $duplicateElements = array_count_values($arr);
    foreach ($duplicateElements as $element => $count) {
        if ($count > 1) {
            echo "Element '$element' is duplicated $count times\n";
        }
    }
}
function AnimalName($components) {
    while (true) {
        $name = '';
        $numberOfComponents = mt_rand(2, 3); // Generate a name with 2-3 components

        for ($i = 0; $i < $numberOfComponents; $i++) {
            $name .= $components[array_rand($components)];
        }
        return trim($name);
    }
}
function createArray()
{   $arr =[];
    $length = mt_rand(3,7);
    for ($i = 0; $i < $length; $i++){
        $arr[$i] = mt_rand(10,20);
    }
    return $arr;
}
function myFunction($arr1, $arr2)
{
   $array = array_merge($arr1,$arr2);
   $uniq = array_unique($array);
   asort($uniq);
   return $uniq;
}

echo(AnimalName($animalComponents) . "<br>");

$tests = myFunction(createArray(),createArray());

foreach ($tests as $key => $val) {
    echo "$val<br>";
}